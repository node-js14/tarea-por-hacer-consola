const Tarea = require('./tarea');
require('colors');

class Tareas {
  _listado = {};

  get listadoArr() {
    const listado = [];
    Object.keys(this._listado).forEach((key) => {
      const tarea = this._listado[key];
      listado.push(tarea);
    });
    return listado;
  }

  constructor() {
    this._listado = {};
  }

  cargarTareasFromArray(tarea = []) {
    tarea.forEach((tarea) => {
      this._listado[tarea.id] = tarea;
    });
  }

  crearTarea(desc = '') {
    const tarea = new Tarea(desc);
    this._listado[tarea.id] = tarea;
  }

  listadoCompleto() {
    console.log();
    this.listadoArr.forEach((item, key) => {
      const { desc, completadoEn } = item;
      const estado = completadoEn ? 'Completado'.green : 'Pendiente'.red;
      const datos = `${key + 1}. ${desc} :: ${estado}`;
      console.log(datos);
    });
  }

  listarPendientesCompletadas(completado = true) {
    console.log();

    this.listadoArr
      .filter((item) =>
        completado ? item.completadoEn : item.completadoEn === null
      )
      .forEach((item, key) => {
        const { desc, completadoEn } = item;
        const estado = completadoEn ? 'Completado'.green : 'Pendiente'.red;
        const datos = `${key + 1}. ${desc} :: ${estado}`;
        console.log(datos);
      });
  }

  toggleCompletadas(ids = []) {
    ids.forEach((id) => {
      const tarea = this._listado[id];
      if (!tarea.completadoEn) {
        tarea.completadoEn = new Date().toISOString();
      }
    });

    this.listadoArr.forEach((tarea) => {
      if (!ids.includes(tarea.id)) {
        this._listado[tarea.id].completadoEn = null;
      }
    });
  }

  borrarTarea(id = '') {
    if (this._listado[id]) {
      delete this._listado[id];
    }
  }
}

module.exports = Tareas;
