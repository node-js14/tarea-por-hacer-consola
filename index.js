require('colors');

const {
  inquirerMenu,
  pausa,
  leerInput,
  listadoTareaBorrar,
  confirmar,
  mostrarListadoCkeckList,
} = require('./helpers/inquirer');

const { saveDB, leerDB } = require('./helpers/saveArchive');

const Tareas = require('./models/tareas');

const main = async () => {
  let opt = '';
  const tareas = new Tareas();

  const tareasDB = leerDB();

  if (tareasDB) {
    tareas.cargarTareasFromArray(tareasDB);
  }
  // await pausa();
  // console.log(tareasDB);
  do {
    opt = await inquirerMenu();
    switch (opt) {
      case '1':
        const desc = await leerInput('Description: ');
        tareas.crearTarea(desc);
        break;
      case '2':
        tareas.listadoCompleto();
        break;
      case '3':
        tareas.listarPendientesCompletadas(true);
        break;
      case '4':
        tareas.listarPendientesCompletadas(null);
        break;
      case '5':
        const ids = await mostrarListadoCkeckList(tareas.listadoArr);
        tareas.toggleCompletadas(ids);
        break;
      case '6':
        const id = await listadoTareaBorrar(tareas.listadoArr);
        if (id !== '0') {
          const ok = await confirmar('Desea eliminar?');
          if (ok) {
            tareas.borrarTarea(id);
            console.log('Tarea borrada'.yellow);
          }
        }
        break;
    }
    saveDB(tareas.listadoArr);

    await pausa();
  } while (opt !== '0');
};

main();
